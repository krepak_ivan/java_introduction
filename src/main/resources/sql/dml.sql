create table student(
	id int primary key,
	name varchar(255),
	phone varchar(100),
	groupId int
);
create table university_group(
	id int primary key,
	name varchar(255)
);
insert into university_group(id, name)
values (1, 'CS-1901'), (2, 'CS-1902'), (3, 'CS-1903'), (4, 'CS-1904'), (5, 'CS-1905');
insert into student(id, name, phone, groupId)
values (11, 'Daniyar', '+7 778 131 82 53', 4), (12, 'Kamila', '+7 707 574 46 34', 4),
(13, 'Aktilek', '+7 702 626 57 83', 3), (14, 'Venera', '+7 776 174 28 53', 2),
(15, 'Kenzhekhan', '+7 778 766 56 89', 1);
select * from student;
select * from university_group;
//test