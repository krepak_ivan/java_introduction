import java.sql.*;

public class Main {
    public static void main(String[] args){
        final String jdbcDriver = "com.mysql.jdbc.Driver";
        final String url = "jdbc:postgresql://localhost:5432/university";

        final String user = "postgres";
        final String password = "123";

        Connection connection = null;
        Statement statement = null;

        try{
            Class.forName(jdbcDriver);
            System.out.println("Trying to connect.");
            connection = DriverManager.getConnection(url, user, password);
            System.out.println("Successful connection.");

            statement = connection.createStatement();
            String sqlStudent = "select * from student";
            ResultSet rs = statement.executeQuery(sqlStudent);

            if(rs != null) System.out.println("Table student");

            Student student = new Student();
            while(rs.next()){
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("name"));
                student.setPhone(rs.getString("phone"));
                student.setGroupId(rs.getInt("groupId"));
                System.out.println(student.toString());
            }

            String sqlGroups = "select * from university_group";
            ResultSet rsGroups = statement.executeQuery(sqlGroups);

            if(rsGroups != null) System.out.println("Table groups");

            Group group = new Group();
            while(rsGroups.next()){
                group.setId(rsGroups.getInt("id"));
                group.setName(rsGroups.getString("name"));
                System.out.println(group.toString());
            }


            Group group_with_students = new Group();
            Student student_of_the_group = new Student();
            ResultSet rsGroupWithStudents = statement.executeQuery(sqlGroups);
            ResultSet rsStudentsOfTheGroup = statement.executeQuery(sqlStudent);
            if(rsGroupWithStudents != null) System.out.println("Students by group");
            if(rsStudentsOfTheGroup == null) System.out.println("There is no students in the university.");

            while(rsGroupWithStudents.next()){
                group_with_students.setName(rsGroupWithStudents.getString("name"));
                group_with_students.setId(rsGroupWithStudents.getInt("id"));
                String tempStudentsOfGroup = " - ";

                while(rsStudentsOfTheGroup.next()){
                    student_of_the_group.setName(rsStudentsOfTheGroup.getString("name"));
                    student_of_the_group.setGroupId(rsStudentsOfTheGroup.getInt("groupId"));

                    if(group_with_students.getId() == student_of_the_group.getGroupId()){
                        tempStudentsOfGroup = tempStudentsOfGroup + student_of_the_group.getName() + ", ";
                    }
                }

                rsStudentsOfTheGroup = statement.executeQuery(sqlStudent);

                if(tempStudentsOfGroup.length() <= 3){
                    System.out.println(group_with_students.getName() + " - there is no students");
                } else System.out.println(group_with_students.getName() + tempStudentsOfGroup);
            }


            rs.close();
        }catch (SQLException se){
            se.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(statement != null) connection.close();
            }catch (SQLException se){}
            try{
                if(connection != null) connection.close();
            }catch (SQLException se){
                se.printStackTrace();
            }
        }
    }
}