public class Group {
    private int id;
    private String name;

    public Group(){}

    @Override
    public String toString(){
        return String.format("id is " + id + ", group name is " + name);
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}