public class Student {
    private int id;
    private String name;
    private String phone;
    private int groupId;

    public Student() {}

    @Override
    public String toString(){
        return String.format("id is " + id + ", name is " + name + ", phone is " + phone + ", group id is " + groupId);
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public int getGroupId() {
        return groupId;
    }
}